### Summary

(_What_ happened?)


### Expected Behavior

(What _should_ happen.)


### Observed Behavior

(What _actually_ happens.)


### Steps to Reproduce

(Step-by-step instructions on how to reproduce the problem without your personal assistance - very important. Be _specific_.)

1.


### Environment and System Information

(Include any relevant information you're able to get. For Java versions, using the `java -version` command at the system prompt/shell should be enough. The fields are listed in order of priority/importance.)

(Feel free to remove rows that are not relevant/applicable. Display and GPU information is only relevant if it's a rendering problem.)

|     Field                                       |                  Value                        |
| :---------------                                | :------------------------------------------   |
| Most Recent Engine Version _without_ the Issue  | (git commit hash/tag, if known)               |
| OS Name/Version/Architecture                    | (e.g. Ubuntu 16.10 x64, Windows 7 x64)        |
| Java Version (JDK)                              | (OpenJDK 1.8.0_121)                           |
| Java Version (JVM)                              | (OpenJDK 64-Bit Server VM build 25.121-b13)   |
| GPU Name/Memory (in GBs)                        | (NVIDIA GTX 960M 2GBs)                        |
| GPU Driver Version                              | (v375.39, etc)                                |
| Display Manager                                 | (Explorer, Gnome, KDE, etc.)                  |
| Display Resolution                              | (1920 x 1080 @60Hz)                           |


### Other Notes and/or Workarounds

(Anything else you think is relevant, but not covered in other sections. State if you've found a way to work around the problem.)


### References

(Any relevant resources/references on the topic.)


### Error Logs and/or Screenshots

(Any properly-formatted logs, stack traces, and/or screenshots - especially if they're rendering issues.)
