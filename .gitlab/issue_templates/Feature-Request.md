### Summary

(State _what_ you think can be added, improved, made easier, etc.)


### Example

(Describe _how_ this would be used. Add pseudo-code if you think it'd help clarify your point(s).)


### Benefits

(Explain _what benefits_ this feature would provide; _why_ is it important?)
