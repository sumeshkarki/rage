/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.math;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

import ray.rml.*;

public class TransformTest {

    @Test
    public void testFactoryMatchesDefault() {
        Transform t = new Transform();

        assertTrue(t.position().equals(Point3f.origin()));
        assertTrue(t.rotation().equals(Matrix3f.identity()));
        assertTrue(t.scale().equals(new Vector3f(1, 1, 1)));
    }

    @Test
    public void testFactoryMatchesParam1() {
        Transform t = new Transform(new Point3f(1, 0, 0));

        assertTrue(t.position().equals(new Point3f(1, 0, 0)));
        assertTrue(t.rotation().equals(Matrix3f.identity()));
        assertTrue(t.scale().equals(new Vector3f(1, 1, 1)));
    }

    @Test
    public void testFactoryMatchesParam2() {
        Transform t = new Transform(new Point3f(1, 0, 0), Matrix3f.identity());

        assertTrue(t.position().equals(new Point3f(1, 0, 0)));
        assertTrue(t.rotation().equals(Matrix3f.identity()));
        assertTrue(t.scale().equals(new Vector3f(1, 1, 1)));
    }

    @Test
    public void testFactoryMatchesParam3() {
        // @formatter:off
        Transform t = new Transform(
            new Point3f(1, 0, 0),
            Matrix3f.identity(),
            Vector3f.unitY()
        );
        // @formatter:on

        assertTrue(t.position().equals(new Point3f(1, 0, 0)));
        assertTrue(t.rotation().equals(Matrix3f.identity()));
        assertTrue(t.scale().equals(Vector3f.unitY()));
    }

    @Test
    public void testFactoryMatchesParamXform() {
        Transform t1 = new Transform();
        Transform t2 = new Transform(t1);

        assertTrue(t1.equals(t2));
    }

    @Test
    public void testSetPositionVectorMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;

        Transform t = new Transform();
        t.setPosition(new Point3f(x, y, z));

        assertEquals(t.position().x(), x);
        assertEquals(t.position().y(), y);
        assertEquals(t.position().z(), z);
    }

    @Test
    public void testSetPositionPrimitiveMatches() {
        final float x = 1f;
        final float y = 2f;
        final float z = 3f;

        Transform t = new Transform();
        t.setPosition(x, y, z);

        assertEquals(t.position().x(), x);
        assertEquals(t.position().y(), y);
        assertEquals(t.position().z(), z);
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testSetPositionNullRejected() {
        Transform t = new Transform();

        t.setPosition(null);
    }

    @Test
    public void testSetRotationMatrixMatches() {
        Matrix3 rotation = Matrix3f.rotation(new Degreef(90), Vector3f.unitY());

        Transform t = new Transform();
        t.setRotation(rotation);

        assertTrue(t.rotation().equals(rotation));
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testSetRotationMatrixNullRejected() {
        Transform t = new Transform();

        t.setRotation(null);
    }

    @Test
    public void testSetScalingVectorMatches() {
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;

        Transform t = new Transform();
        t.setScale(new Vector3f(x, y, z));

        assertEquals(t.scale().x(), x);
        assertEquals(t.scale().y(), y);
        assertEquals(t.scale().z(), z);
    }

    @Test
    public void testSetScalingPrimitiveMatches() {
        final float x = 2f;
        final float y = 4f;
        final float z = 6f;

        Transform t = new Transform();
        t.setScale(x, y, z);

        assertEquals(t.scale().x(), x);
        assertEquals(t.scale().y(), y);
        assertEquals(t.scale().z(), z);
    }

    @Test(expectedExceptions = { NullPointerException.class })
    public void testSetScalingNullRejected() {
        Transform t = new Transform();

        t.setScale(null);
    }

    @Test
    public void testEqualityContractReflexivity() {
        // For any non-null reference value x, x.equals(x) must return true.
        Transform t = new Transform();

        assertTrue(t.equals(t));
        assertFalse(t.equals(null));
    }

    @Test
    public void testEqualityContractSymmetry() {
        // For any non-null reference values x and y, x.equals(y) must re-
        // turn true if and only if y.equals(x) returns true.
        Transform t1 = new Transform();
        Transform t2 = new Transform();

        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));

        assertTrue(t1.hashCode() == t2.hashCode());

        assertFalse(t1.equals(null));
        assertFalse(t2.equals(null));
    }

    @Test
    public void testEqualityContractTransitivity() {
        // For any non-null reference values x, y, z, if x.equals(y)
        // returns true and y.equals(z) returns true, then x.equals(z) must
        // return true.
        Transform t1 = new Transform();
        Transform t2 = new Transform();
        Transform t3 = new Transform();

        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t3));
        assertTrue(t1.equals(t3));

        assertTrue(t1.hashCode() == t2.hashCode());
        assertTrue(t2.hashCode() == t3.hashCode());
        assertTrue(t1.hashCode() == t3.hashCode());

        assertFalse(t1.equals(null));
        assertFalse(t2.equals(null));
        assertFalse(t3.equals(null));
    }

    @Test
    public void testEqualityContractConsistency() {
        // For any non-null reference values x and y, multiple invocations of
        // x.equals(y) consistently return true or consistently return false,
        // provided no information used in equals comparisons on the objects is
        // modified.
        Transform t1 = new Transform();
        Transform t2 = new Transform();

        assertTrue(t1.equals(t2));
        assertTrue(t1.equals(t2));

        assertTrue(t1.hashCode() == t2.hashCode());
        assertTrue(t1.hashCode() == t2.hashCode());

        t1.setPosition(1, 2, 3);
        assertFalse(t1.equals(t2));
        assertFalse(t1.hashCode() == t2.hashCode());

        assertFalse(t1.equals(null));
        assertFalse(t2.equals(null));
    }

    @Test
    public void testEqualityContractForNull() {
        assertFalse(new Transform().equals(null));
    }

}
