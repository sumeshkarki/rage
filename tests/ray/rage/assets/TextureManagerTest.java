/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.assets;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

import ray.rage.asset.texture.*;

public class TextureManagerTest {

    private static final String ASSET_NAME = "MainAsset";

    @Test
    public void testCanCreateManualTexture() {
        TextureManager tm = new TextureManager();
        Texture t = tm.createManualAsset(ASSET_NAME);

        assertEquals(ASSET_NAME, t.getName());
    }

    @Test(
        expectedExceptions = { RuntimeException.class },
        expectedExceptionsMessageRegExp = "Manual asset already exists: " + ASSET_NAME)
    public void testThrowsExceptionOnDuplicateTexture() {
        TextureManager mm = new TextureManager();
        mm.createManualAsset(ASSET_NAME);
        mm.createManualAsset(ASSET_NAME);
    }

    @Test
    public void testFindsTextureByName() {
        TextureManager mm = new TextureManager();
        mm.createManualAsset(ASSET_NAME);

        assertTrue(mm.hasAssetByName(ASSET_NAME));
    }

    @Test
    public void testDoesNotFindTextureByName() {
        TextureManager mm = new TextureManager();
        assertFalse(mm.hasAssetByName(ASSET_NAME));
    }

    @Test
    public void testMeshCountMatches() {
        TextureManager tm = new TextureManager();

        assertEquals(0, tm.getAssetCount());
        tm.createManualAsset(ASSET_NAME);
        assertEquals(1, tm.getAssetCount());
    }

}
