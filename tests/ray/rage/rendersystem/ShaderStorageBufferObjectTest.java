/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.rendersystem;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.nio.*;
import java.util.*;
import java.util.Timer;

import javax.swing.*;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.*;
import com.jogamp.opengl.util.glsl.*;

import ray.rage.asset.mesh.*;
import ray.rage.asset.mesh.loaders.*;
import ray.rml.*;

/**
 * A test to give me a better understanding of what's necessary to get a working
 * SSBO. OpenGL 4.3 or later is required for SSBO support.
 *
 * @author Raymond L. Rivera
 *
 */
public class ShaderStorageBufferObjectTest implements GLEventListener, KeyListener {

    private static final int    INVALID_ID    = -1;

    private static final String MESH_NAME     = "sphere.obj";
    private static final float  MESH_SCALE    = .5f;

    private Timer               timer         = new Timer();
    private JFrame              frame         = new JFrame(ShaderStorageBufferObjectTest.class.getName());
    private final GLCanvas      canvas        = new GLCanvas();

    private ShaderProgram       ssboProgram   = new ShaderProgram();

    private MeshManager         meshManager   = new MeshManager();

    FloatBuffer                 vertices;
    FloatBuffer                 normals;
    IntBuffer                   indices;

    private int[]               vaos          = new int[1];
    private int[]               vbos          = new int[3];
    private int[]               ssbos         = new int[1];

    private int                 modelMatrixId = INVALID_ID;
    private int                 viewMatrixId  = INVALID_ID;
    private int                 projMatrixId  = INVALID_ID;
    private int                 normMatrixId  = INVALID_ID;

    private Matrix4             modelMatrix   = Matrix4f.translation(0, 0, -7);
    private Matrix4             viewMatrix    = getViewMatrix();
    private Matrix4             projMatrix    = Matrix4f.identity();

    private Light[]             lights;

    public ShaderStorageBufferObjectTest() {
        canvas.addGLEventListener(this);
        canvas.addKeyListener(this);

        meshManager.addAssetLoader(new WavefrontMeshLoader());
        try {
            final String name = "assets/meshes/" + MESH_NAME;
            meshManager.getAssetByPath(name.replace('/', File.separatorChar));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        modelMatrix = modelMatrix.scale(MESH_SCALE, MESH_SCALE, MESH_SCALE);

        // @formatter:off
        lights = new Light[] {
            new Light(Color.BLACK, Color.RED, Color.RED, new Vector4f(-15, 5, 5)),
            new Light(Color.BLACK, Color.BLUE, Color.BLUE, new Vector4f(15, 5, 5))
        };
        // @formatter:on

        frame.setSize(1024, 768);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(canvas, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                canvas.display();
            }
        }, 0, 16); // draw every 16ms, for 60 FPS

        frame.setVisible(true);
    }

    @Override
    public void init(GLAutoDrawable glad) {
        GL4 gl = (GL4) glad.getGL();

        gl.glEnable(GL4.GL_CULL_FACE);
        gl.glClearColor(0, 0, 0, 1);
        gl.glClearDepthf(1f);

        gl.glGenVertexArrays(vaos.length, vaos, 0);
        gl.glBindVertexArray(vaos[0]);

        buildProgram(gl);
        ssboProgram.useProgram(gl, true);
        int pid = ssboProgram.program();

        modelMatrixId = gl.glGetUniformLocation(pid, "matrix.model");
        viewMatrixId = gl.glGetUniformLocation(pid, "matrix.view");
        projMatrixId = gl.glGetUniformLocation(pid, "matrix.projection");
        normMatrixId = gl.glGetUniformLocation(pid, "matrix.normal");

        createVBOs(gl, pid);
        createLightSSBO(gl, pid);
        // createLightSSBO2(gl, pid);
    }

    @Override
    public void display(GLAutoDrawable glad) {
        GL4 gl = glad.getGL().getGL4();

        gl.glClear(GL4.GL_COLOR_BUFFER_BIT | GL4.GL_DEPTH_BUFFER_BIT);

        gl.glUniformMatrix4fv(modelMatrixId, 1, false, modelMatrix.toFloatArray(), 0);
        gl.glUniformMatrix4fv(viewMatrixId, 1, false, viewMatrix.toFloatArray(), 0);
        gl.glUniformMatrix4fv(projMatrixId, 1, false, projMatrix.toFloatArray(), 0);

        Matrix4 normalMatrix = viewMatrix.mult(modelMatrix).inverse().transpose();
        gl.glUniformMatrix4fv(normMatrixId, 1, false, normalMatrix.toFloatArray(), 0);

        gl.glDrawElements(GL4.GL_TRIANGLES, indices.capacity(), GL4.GL_UNSIGNED_INT, 0);
    }

    private void createVBOs(GL4 gl, int pid) {
        Mesh mesh = meshManager.getAssetByName(MESH_NAME);
        SubMesh subMesh = mesh.getSubMesh(0);

        gl.glGenBuffers(vbos.length, vbos, 0);

        vertices = subMesh.getVertexBuffer();
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[0]);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, vertices.capacity() * Float.BYTES, vertices, GL4.GL_STATIC_DRAW);
        gl.glVertexAttribPointer(/* layout (location = */ 0, /* vec3 = */ 3 /* floats */, GL4.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(0);

        normals = subMesh.getNormalBuffer();
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[1]);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, normals.capacity() * Float.BYTES, normals, GL4.GL_STATIC_DRAW);
        gl.glVertexAttribPointer(/* layout (location = */ 1, /* vec3 = */ 3 /* floats */, GL4.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(1);

        indices = subMesh.getIndexBuffer();
        gl.glBindBuffer(GL4.GL_ELEMENT_ARRAY_BUFFER, vbos[2]);
        gl.glBufferData(GL4.GL_ELEMENT_ARRAY_BUFFER, indices.capacity() * Integer.BYTES, indices, GL4.GL_STATIC_DRAW);
    }

    private void createLightSSBO(GL4 gl, int pid) {
        final int capacity = getBufferCapacity(lights);
        FloatBuffer buff = FloatBuffer.allocate(capacity);
        for (int i = 0; i < lights.length; ++i) {
            buff.put(lights[i].ambient.toFloatArray());
            buff.put(lights[i].diffuse.toFloatArray());
            buff.put(lights[i].specular.toFloatArray());
            buff.put(lights[i].position.toFloatArray());
        }
        buff.rewind();

        gl.glGenBuffers(ssbos.length, ssbos, 0);
        gl.glBindBufferBase(GL4.GL_SHADER_STORAGE_BUFFER, /* binding = */0, ssbos[0]);
        gl.glBufferData(GL4.GL_SHADER_STORAGE_BUFFER, capacity * Float.BYTES, buff, GL4.GL_STATIC_DRAW);
    }

    // This is equivalent to createLightSSBO, but using
    // glMapBuffer/glUnmapBuffer instead
    private void createLightSSBO2(GL4 gl, int pid) {
        final int capacity = getBufferCapacity(lights);

        gl.glGenBuffers(ssbos.length, ssbos, 0);
        gl.glBindBufferBase(GL4.GL_SHADER_STORAGE_BUFFER, /* binding = */0, ssbos[0]);

        // cause GPU to allocate memory, but don't send data yet
        gl.glBufferData(GL4.GL_SHADER_STORAGE_BUFFER, capacity * Float.BYTES, null, GL4.GL_STATIC_DRAW);

        // map GPU server memory to client address space to put the values in
        FloatBuffer buff = gl.glMapBuffer(GL4.GL_SHADER_STORAGE_BUFFER, GL4.GL_WRITE_ONLY).asFloatBuffer();
        for (int i = 0; i < lights.length; ++i) {
            buff.put(lights[i].ambient.toFloatArray());
            buff.put(lights[i].diffuse.toFloatArray());
            buff.put(lights[i].specular.toFloatArray());
            buff.put(lights[i].position.toFloatArray());
        }
        buff.rewind();
        gl.glUnmapBuffer(GL4.GL_SHADER_STORAGE_BUFFER);
    }

    private static String getVertexSource() {
        // @formatter:off
        return
            "#version 430 core                                                                               \n"
            + "                                                                                              \n"
            + "layout (location = 0) in vec3 vertex_position;                                                \n"
            + "layout (location = 1) in vec3 vertex_normal;                                                  \n"
            + "                                                                                              \n"
            + "out vertex_t {                                                                                \n"
            + "    vec3 position; // in view-space                                                           \n"
            + "    vec3 normal;   // in view-space                                                           \n"
            + "} vs;                                                                                         \n"
            + "                                                                                              \n"
            + "uniform struct matrix_t {                                                                     \n"
            + "    mat4 model;                                                                               \n"
            + "    mat4 view;                                                                                \n"
            + "    mat4 projection;                                                                          \n"
            + "    mat4 normal;                                                                              \n"
            + "} matrix;                                                                                     \n"
            + "                                                                                              \n"
            + "void main() {                                                                                 \n"
            + "    vec4 position = vec4(vertex_position, 1);                                                 \n"
            + "    gl_Position   = matrix.projection * matrix.view * matrix.model * position;                \n"
            + "    vs.position   = (matrix.view * matrix.model * position).xyz;                              \n"
            + "    vs.normal     = mat3(matrix.normal) * vertex_normal;                                      \n"
            + "}                                                                                             \n";
        // @formatter:on
    }

    private static String getFragmentSource() {
        // @formatter:off
        return
            "#version 430 core                                                                               \n"
            + "                                                                                              \n"
            + "in vertex_t {                                                                                 \n"
            + "    vec3 position;                                                                            \n"
            + "    vec3 normal;                                                                              \n"
            + "} fs;                                                                                         \n"
            + "                                                                                              \n"
            + "out vec4 frag;                                                                                \n"
            + "                                                                                              \n"
            + "const vec4 global_ambient = vec4(.1, .1, .1, 1);                                              \n"
            + "                                                                                              \n"
            + "struct material_t {                                                                           \n"
            + "    vec4  ambient;                                                                            \n"
            + "    vec4  diffuse;                                                                            \n"
            + "    vec4  specular;                                                                           \n"
            + "    vec4  emission;                                                                           \n"
            + "    float shininess;                                                                          \n"
            + "};                                                                                            \n"
            + "                                                                                              \n"
            + "const material_t mat = material_t(       // silver material                                   \n"
            + "    vec4(0.1923f, 0.1923f, 0.1923f, 1),  // ambient                                           \n"
            + "    vec4(0.5075f, 0.5075f, 0.5075f, 1),  // diffuse                                           \n"
            + "    vec4(0.5083f, 0.5083f, 0.5083f, 1),  // specular                                          \n"
            + "    vec4(0.0f   , 0.0f   , 0.0f   , 1),  // emission                                          \n"
            + "    51.2                                 // shininess                                         \n"
            + ");                                                                                            \n"
            + "                                                                                              \n"
            + "struct light_t {                                                                              \n"
            + "    vec4 ambient;                                                                             \n"
            + "    vec4 diffuse;                                                                             \n"
            + "    vec4 specular;                                                                            \n"
            + "    vec4 position;                                                                            \n"
            + "};                                                                                            \n"
            + "                                                                                              \n"
            + "// OpenGL 4.3 is the bare-minimum for Shader Storage Buffer Object support.                   \n"
            + "layout(std430, binding = 0) buffer ssbo_t {                                                   \n"
            + "    light_t lights[];                                                                         \n"
            + "} ssbo;                                                                                       \n"
            + "                                                                                              \n"
            + "void main() {                                                                                 \n"
            + "                                                                                              \n"
            + "    frag = vec4(1, 1, 1, 1);  // simulate a white texture w/o using tex sampler               \n"
            + "    vec4 effect;                                                                              \n"
            + "                                                                                              \n"
            + "    for (int i = 0; i < ssbo.lights.length(); ++i) {                                          \n"
            + "        light_t light = ssbo.lights[i];                                                       \n"
            + "                                                                                              \n"
            + "        vec3 N = normalize(fs.normal);                                                        \n"
            + "        vec3 L = normalize(light.position.xyz - fs.position);  // using point-light           \n"
            + "        vec3 V = normalize(-fs.position);                                                     \n"
            + "        vec3 H = normalize(L + V);                                                            \n"
            + "                                                                                              \n"
            + "        vec4 ambient  = light.ambient  * mat.ambient  + mat.ambient * global_ambient;         \n"
            + "        vec4 diffuse  = light.diffuse  * mat.diffuse  * max(dot(N, L), 0);                    \n"
            + "        vec4 specular = light.specular * mat.specular * pow(max(dot(N, H), 0), mat.shininess);\n"
            + "                                                                                              \n"
            + "        // scale 'texture' color with material/light contributions                            \n"
            + "        effect += ambient + diffuse + specular + mat.emission;                                \n"
            + "    }                                                                                         \n"
            + "    frag *= effect;                                                                           \n"
            + "}                                                                                             \n";
        // @formatter:on
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE:
                cleanup();
                System.exit(0);
                break;
        }
    }

    public static void main(String[] args) {
        new ShaderStorageBufferObjectTest();
    }

    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
        GL4 gl = glad.getGL().getGL4();
        gl.glViewport(x, y, width, height);

        projMatrix = Matrix4f.perspective(new Degreef(60f), width / height, 0.1f, 100f);
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        GL4 gl = (GL4) glad.getGL();
        ssboProgram.destroy(gl);
        gl.glDeleteVertexArrays(vaos.length, vaos, 0);
        gl.glDeleteBuffers(vbos.length, vbos, 0);
        gl.glDeleteBuffers(ssbos.length, ssbos, 0);
    }

    private void buildProgram(GL4 gl) {
        ShaderCode vs = createShader(gl, GL4.GL_VERTEX_SHADER, getVertexSource());
        ShaderCode fs = createShader(gl, GL4.GL_FRAGMENT_SHADER, getFragmentSource());

        ssboProgram.init(gl);
        ssboProgram.add(vs);
        ssboProgram.add(fs);

        ssboProgram.link(gl, System.err);
        if (!ssboProgram.validateProgram(gl, System.err))
            throw new RuntimeException("Program failed to link");

        vs.destroy(gl);
        fs.destroy(gl);
    }

    private ShaderCode createShader(GL4 gl, int shaderType, String source) {
        String[][] sources = new String[1][1];
        sources[0] = new String[] { source };

        ShaderCode shader = new ShaderCode(shaderType, sources.length, sources);

        if (!shader.compile(gl, System.err))
            throw new RuntimeException("Shader compilation failed\n" + source);

        return shader;
    }

    private void cleanup() {
        canvas.disposeGLEventListener(this, true);
        meshManager.notifyDispose();
        vertices.clear();
        normals.clear();
        indices.clear();
    }

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void keyTyped(KeyEvent e) {}

    private static Matrix4 getViewMatrix() {
        Vector4 position = new Vector4f(0, 0, 5);
        Vector4 forward = Vector4f.unitZ();
        Vector4 up = Vector4f.unitY();
        return Matrix4f.lookAt(position, forward, up);
    }

    private static int getBufferCapacity(Light[] lights) {
        return lights[0].getSizeAsBytes() * lights.length;
    }

    /**
     * Simple utility class for light information.
     */
    class Light {

        public Vector4 ambient;
        public Vector4 diffuse;
        public Vector4 specular;
        public Vector4 position;

        public Light(Color amb, Color diff, Color spec, Vector4 pos) {
            ambient = toVec4(amb.getColorComponents(null));
            diffuse = toVec4(diff.getColorComponents(null));
            specular = toVec4(spec.getColorComponents(null));
            position = pos;
        }

        // Need this because Java has no equivalent sizeof operator, like C/C++
        // to figure out the in-memory size, in bytes, of an object. Thank you,
        // James Gosling... 🙄😒😠👿
        public int getSizeAsBytes() {
            // 4 floats per Vector4 instance; 4 Vector4 instances
            return 16;
        }

        private Vector4 toVec4(float[] rgb) {
            return new Vector4f(rgb[0], rgb[1], rgb[2], 1);
        }

    }

}
