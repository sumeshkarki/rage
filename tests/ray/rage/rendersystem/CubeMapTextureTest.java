/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.rendersystem;

import java.awt.*;
import java.awt.color.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;
import java.util.Timer;

import javax.imageio.*;
import javax.swing.*;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.*;
import com.jogamp.opengl.util.glsl.*;

import ray.rage.util.*;
import ray.rml.*;

/**
 * A test to give me a better understanding of what's necessary to get a working
 * SkyBox.
 *
 * @author Raymond L. Rivera
 *
 */
public class CubeMapTextureTest implements GLEventListener, KeyListener {

    private static final int INVALID_ID       = -1;

    private Timer            timer            = new Timer();
    private JFrame           frame            = new JFrame(CubeMapTextureTest.class.getName());
    private final GLCanvas   canvas           = new GLCanvas();

    private ShaderProgram    skyboxProgram    = new ShaderProgram();

    private int[]            vaos             = new int[1];
    private int[]            vbos             = new int[2];
    private int[]            tbos             = new int[1];

    private int              vertexPositionId = INVALID_ID;
    private int              viewMatrixId     = INVALID_ID;
    private int              projMatrixId     = INVALID_ID;

    private IntBuffer        indexBuff;

    private Angle            rotationAngle    = new Degreef(2.5f);

    private Matrix4          projMatrix       = Matrix4f.identity();
    private Matrix4          viewMatrix       = getViewMatrix();

    public CubeMapTextureTest() {
        canvas.addGLEventListener(this);
        canvas.addKeyListener(this);

        frame.setSize(1024, 768);
        frame.setLocationRelativeTo(null);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(canvas, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                canvas.display();
            }
        }, 0, 16); // draw every 16ms, for 60 FPS
    }

    @Override
    public void init(GLAutoDrawable glad) {
        GL4 gl = (GL4) glad.getGL();

        gl.glEnable(GL4.GL_TEXTURE_CUBE_MAP_SEAMLESS);
        gl.glDisable(GL4.GL_DEPTH_TEST);

        gl.glClearColor(0, 0, 0, 1f);
        gl.glClearDepthf(1f);

        gl.glGenVertexArrays(vaos.length, vaos, 0);
        gl.glGenBuffers(vbos.length, vbos, 0);
        gl.glGenTextures(tbos.length, tbos, 0);

        gl.glBindVertexArray(vaos[0]);

        buildSkyBoxProgram(gl);
        buildSkyBox(gl);

        int pid = skyboxProgram.program();
        vertexPositionId = gl.glGetAttribLocation(pid, "vertex_position");
        viewMatrixId = gl.glGetUniformLocation(pid, "view_matrix");
        projMatrixId = gl.glGetUniformLocation(pid, "proj_matrix");

        FloatBuffer vb = getVertexBuffer(64, 64);
        gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vbos[0]);
        gl.glBufferData(GL4.GL_ARRAY_BUFFER, vb.capacity() * Float.BYTES, vb, GL4.GL_STATIC_DRAW);
        gl.glVertexAttribPointer(vertexPositionId, 3, GL4.GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(vertexPositionId);

        IntBuffer ib = getIndexBuffer();
        gl.glBindBuffer(GL4.GL_ELEMENT_ARRAY_BUFFER, vbos[1]);
        gl.glBufferData(GL4.GL_ELEMENT_ARRAY_BUFFER, ib.capacity() * Integer.BYTES, ib, GL4.GL_STATIC_DRAW);

        indexBuff = ib;
        skyboxProgram.useProgram(gl, true);
    }

    @Override
    public void display(GLAutoDrawable glad) {
        GL4 gl = glad.getGL().getGL4();

        gl.glClear(GL4.GL_COLOR_BUFFER_BIT | GL4.GL_DEPTH_BUFFER_BIT);

        gl.glUniformMatrix4fv(viewMatrixId, 1, false, viewMatrix.toFloatArray(), 0);
        gl.glUniformMatrix4fv(projMatrixId, 1, false, projMatrix.toFloatArray(), 0);

        // if other objects need to be rendered within the skybox, then depth
        // testing should be disabled for skybox rendering, and it should be
        // rendered first, and then re-enabled before rendering the other
        // objects
        gl.glDrawElements(GL4.GL_TRIANGLE_STRIP, indexBuff.capacity(), GL4.GL_UNSIGNED_INT, 0);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
        GL4 gl = glad.getGL().getGL4();
        gl.glViewport(x, y, width, height);

        projMatrix = Matrix4f.perspective(new Degreef(60f), width / height, 0.1f, 100f);
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        GL4 gl = (GL4) glad.getGL();
        skyboxProgram.destroy(gl);
        gl.glDeleteVertexArrays(vaos.length, vaos, 0);
        gl.glDeleteBuffers(vbos.length, vbos, 0);
        gl.glDeleteTextures(tbos.length, tbos, 0);
    }

    private void buildSkyBox(GL4 gl) {
        // @formatter:off
        final int[] targets = new int[] {
            GL4.GL_TEXTURE_CUBE_MAP_POSITIVE_X,
            GL4.GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
            GL4.GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
            GL4.GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
            GL4.GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
            GL4.GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
        };
        final Texture[] textures = new Texture[] {
            loadTexture("right.png"),
            loadTexture("left.png"),
            loadTexture("top.png"),
            loadTexture("bottom.png"),
            loadTexture("front.png"),
            loadTexture("back.png")
        };
        // @formatter:on

        final int width = textures[0].getImage().getWidth();
        final int height = textures[0].getImage().getHeight();

        gl.glActiveTexture(GL4.GL_TEXTURE0);
        gl.glBindTexture(GL4.GL_TEXTURE_CUBE_MAP, tbos[0]);
        gl.glTexStorage2D(GL4.GL_TEXTURE_CUBE_MAP, 1, GL4.GL_RGBA8, width, height);

        for (int i = 0; i < textures.length; ++i) {
            // @formatter:off
            gl.glTexSubImage2D(
                targets[i],
                0,	// MIPmap level
                0,	// x-offset
                0,	// y-offset
                textures[i].getImage().getWidth(),
                textures[i].getImage().getHeight(),
                GL4.GL_RGBA,
                GL4.GL_UNSIGNED_BYTE,
                textures[i].getByteBuffer()
            );
            // @formatter:on
            gl.glTexParameteri(GL4.GL_TEXTURE_CUBE_MAP, GL4.GL_TEXTURE_MAG_FILTER, GL4.GL_LINEAR);
            gl.glTexParameteri(GL4.GL_TEXTURE_CUBE_MAP, GL4.GL_TEXTURE_MIN_FILTER, GL4.GL_LINEAR);
            gl.glTexParameteri(GL4.GL_TEXTURE_CUBE_MAP, GL4.GL_TEXTURE_WRAP_S, GL4.GL_CLAMP_TO_EDGE);
            gl.glTexParameteri(GL4.GL_TEXTURE_CUBE_MAP, GL4.GL_TEXTURE_WRAP_T, GL4.GL_CLAMP_TO_EDGE);
            gl.glTexParameteri(GL4.GL_TEXTURE_CUBE_MAP, GL4.GL_TEXTURE_WRAP_R, GL4.GL_CLAMP_TO_EDGE);
        }
    }

    private void buildSkyBoxProgram(GL4 gl) {
        ShaderCode vs = createShader(gl, GL4.GL_VERTEX_SHADER, getVertexSource());
        ShaderCode fs = createShader(gl, GL4.GL_FRAGMENT_SHADER, getFragmentSource());

        skyboxProgram.init(gl);
        skyboxProgram.add(vs);
        skyboxProgram.add(fs);

        skyboxProgram.link(gl, System.err);
        if (!skyboxProgram.validateProgram(gl, System.err))
            throw new RuntimeException("SkyBox program failed to link");

        vs.destroy(gl);
        fs.destroy(gl);
    }

    private ShaderCode createShader(GL4 gl, int shaderType, String source) {
        String[][] sources = new String[1][1];
        sources[0] = new String[] { source };

        ShaderCode shader = new ShaderCode(shaderType, sources.length, sources);

        if (!shader.compile(gl, System.err))
            throw new RuntimeException("Shader compilation failed\n" + source);

        return shader;
    }

    private Texture loadTexture(String filename) {
        final String skyboxesDir = "assets/skyboxes/oga/galaxy/red/dense/".replace('/', File.separatorChar);

        BufferedImage img;
        try {
            img = ImageIO.read(Files.newInputStream(Paths.get(skyboxesDir + filename)));
        } catch (IOException e) {
            throw new RuntimeException(e.getLocalizedMessage(), e);
        }
        img = toFlippedRGBA(img);

        Texture t = new Texture();
        t.setImage(img);
        return t;
    }

    private static BufferedImage toFlippedRGBA(BufferedImage orig) {
        final int dataBufferType = DataBuffer.TYPE_BYTE;
        final int[] rgbaBits = new int[] { 8, 8, 8, 8 };

        // @formatter:off
        WritableRaster raster = Raster.createInterleavedRaster(
            dataBufferType,
            orig.getWidth(),
            orig.getHeight(),
            rgbaBits.length,
            null
        );
        ColorModel colorModel = new ComponentColorModel(
            ColorSpace.getInstance(ColorSpace.CS_sRGB),
            rgbaBits,
            true,	// has alpha?
            false,	// pre-multiplied alpha?
            ComponentColorModel.TRANSLUCENT,
            dataBufferType
        );
        BufferedImage xformedImage = new BufferedImage(
            colorModel,
            raster,
            colorModel.isAlphaPremultiplied(),
            null
        );
        // @formatter:on

        Graphics2D gfx = xformedImage.createGraphics();
        gfx.drawImage(orig, null, null);
        gfx.dispose();

        return xformedImage;
    }

    private static String getVertexSource() {
        // @formatter:off
        return
            "#version 450 core\n"
            + "\n"
            + "layout (location = 0) in vec3 vertex_position;\n"
            + "\n"
            + "uniform mat4 view_matrix;\n"
            + "uniform mat4 proj_matrix;\n"
            + "\n"
            + "out vec3 texcoord;\n"
            + "\n"
            + "void main()\n"
            + "{\n"
            + "    texcoord     = mat3(view_matrix) * vertex_position;\n"
            + "    gl_Position  = proj_matrix * vec4(vertex_position, 1.0);\n"
            + "}\n";
        // @formatter:on
    }

    private static String getFragmentSource() {
        // @formatter:off
        return
            "#version 450 core\n"
            + "\n"
            + "in vec3 texcoord;\n"
            + "\n"
            + "out vec4 color;\n"
            + "\n"
            + "layout (binding = 0) uniform samplerCube cubemap_texture;\n"
            + "\n"
            + "void main()\n"
            + "{\n"
            + "    color = texture(cubemap_texture, texcoord);\n"
            + "}\n";
        // @formatter:on
    }

    private static FloatBuffer getVertexBuffer(float w, float h) {
        // @formatter:off
        float[] vertices = new float[] {
            -w, -h, -1f,
             w, -h, -1f,
            -w,  h, -1f,
             w,  h, -1f
        };
        // @formatter:on
        return BufferUtil.directFloatBuffer(vertices);
    }

    private static IntBuffer getIndexBuffer() {
        return BufferUtil.directIntBuffer(new int[] { 0, 1, 2, 3 });
    }

    private static Matrix4 getViewMatrix() {
        Vector4 position = Vector4f.zero();
        Vector4 forward = Vector4f.unitZ();
        Vector4 up = Vector4f.unitY();
        return Matrix4f.lookAt(position, forward, up);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE:
                canvas.disposeGLEventListener(this, true);
                System.exit(0);
                break;
            case KeyEvent.VK_W:
            case KeyEvent.VK_UP:
                viewMatrix = viewMatrix.rotate(rotationAngle, Vector3f.unitX());
                break;
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
                viewMatrix = viewMatrix.rotate(rotationAngle, Vector3f.unitY());
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                viewMatrix = viewMatrix.rotate(rotationAngle.mult(-1), Vector3f.unitX());
                break;
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                viewMatrix = viewMatrix.rotate(rotationAngle.mult(-1), Vector3f.unitY());
                break;
            case KeyEvent.VK_R:
            case KeyEvent.VK_ENTER:
                // reset
                viewMatrix = getViewMatrix();
                break;
        }
    }

    public static void main(String[] args) {
        new CubeMapTextureTest();
    }

    @Override
    public void keyReleased(KeyEvent e) {}

    @Override
    public void keyTyped(KeyEvent e) {}

    /**
     * Convenient class to hold BufferedImage data.
     *
     * @author Raymond L. Rivera
     *
     */
    private class Texture {

        private BufferedImage image;
        private ByteBuffer    rgbaBuffer;

        public void setImage(BufferedImage img) {
            DataBufferByte bytes = (DataBufferByte) img.getRaster().getDataBuffer();
            rgbaBuffer = BufferUtil.directByteBuffer(bytes.getData());
            image = img;
        }

        public BufferedImage getImage() {
            return image;
        }

        public ByteBuffer getByteBuffer() {
            return rgbaBuffer;
        }
    }

}
