/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.controllers.*;
import ray.rml.*;

public class SpotLightTest extends VariableFrameRateGame {

    private final static float LIGHT_RANGE      = 45f;
    private final static float LIGHT_OFFSET     = .5f;
    private final static float SCALE_FACTOR     = 3f;
    private final static float DISTANCE_FACTOR  = SCALE_FACTOR * 3f;
    private final static Angle startConeAngle   = new Degreef(15f);
    private final static Angle specialConeAngle = new Degreef(180);

    private Color              ambientIntensity = new Color(.025f, .025f, .025f);
    private Light              spotLight;
    private SceneNode          cameraNode;
    private SceneNode          cubeTargetNode;
    private SceneNode          lightNode;
    private boolean            autoRotate       = true;

    public SpotLightTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new SpotLightTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        cameraNode = sm.getRootSceneNode().createChildSceneNode(camera.getName() + "Node");
        cameraNode.attachObject(camera);
        cameraNode.setLocalPosition(0, DISTANCE_FACTOR / 2, DISTANCE_FACTOR);
        cameraNode.lookAt(Point3f.origin());
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        SceneNode rootNode = sm.getRootSceneNode();

        Entity cube1 = sm.createEntity("Cube1", "cube_nomat.obj");
        Entity cube2 = sm.createEntity("Cube2", "cube_nomat.obj");
        Entity cube3 = sm.createEntity("Cube3", "cube_nomat.obj");

        SceneNode cubeNode1 = rootNode.createChildSceneNode(cube1.getName() + "Node");
        SceneNode cubeNode2 = rootNode.createChildSceneNode(cube2.getName() + "Node");
        SceneNode cubeNode3 = rootNode.createChildSceneNode(cube3.getName() + "Node");

        cubeNode1.attachObject(cube1);
        cubeNode2.attachObject(cube2);
        cubeNode3.attachObject(cube3);

        cubeNode1.setLocalPosition(0, 0, -DISTANCE_FACTOR);
        cubeNode2.setLocalPosition(-DISTANCE_FACTOR, 0, 0);
        cubeNode3.setLocalPosition(DISTANCE_FACTOR, 0, 0);

        cubeNode1.scale(SCALE_FACTOR);
        cubeNode2.scale(SCALE_FACTOR);
        cubeNode3.scale(SCALE_FACTOR);

        Angle angle = new Degreef(45);
        cubeNode1.yaw(angle);
        cubeNode2.yaw(angle);
        cubeNode3.yaw(angle);

        cubeTargetNode = cubeNode1;

        spotLight = sm.createLight("SpotLight", Light.Type.SPOT);
        spotLight.setConeCutoffAngle(startConeAngle);
        spotLight.setRange(LIGHT_RANGE);

        Entity spotLightMarker = sm.createEntity(spotLight.getName() + "Marker", "cone.obj");
        spotLightMarker.getSubEntity(0).getMaterial().setEmissive(Color.WHITE);

        lightNode = rootNode.createChildSceneNode(spotLight.getName() + "Node");
        lightNode.attachObject(spotLight);
        lightNode.attachObject(spotLightMarker);

        RotationController rc = new RotationController();
        rc.addNode(lightNode);
        sm.addController(rc);

        updateLightColor(Color.WHITE);
        sm.getAmbientLight().setIntensity(ambientIntensity);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_R:
                updateLightColor(Color.RED);
                break;
            case KeyEvent.VK_G:
                updateLightColor(Color.GREEN);
                break;
            case KeyEvent.VK_B:
                updateLightColor(Color.BLUE);
                break;
            case KeyEvent.VK_W:
                updateLightColor(Color.WHITE);
                break;
            case KeyEvent.VK_PLUS:
            case KeyEvent.VK_ADD:
                lightNode.moveForward(LIGHT_OFFSET);
                break;
            case KeyEvent.VK_SUBTRACT:
            case KeyEvent.VK_MINUS:
                lightNode.moveBackward(LIGHT_OFFSET);
                break;
            case KeyEvent.VK_SPACE: {
                autoRotate = !autoRotate;
                getEngine().getSceneManager().getController(0).setEnabled(autoRotate);
                if (!autoRotate)
                    lightNode.lookAt(cubeTargetNode);
                break;
            }
        }
        super.keyPressed(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // avoids ambiguity and also allows the spot's cone range to be tested
        // correctly when using the wheel
        if (SwingUtilities.isLeftMouseButton(e))
            spotLight.setConeCutoffAngle(specialConeAngle);
        else if (SwingUtilities.isRightMouseButton(e))
            spotLight.setConeCutoffAngle(startConeAngle);

        super.mouseClicked(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        Angle delta = new Degreef(2.5f);
        Angle cutoff = spotLight.getConeCutoffAngle();

        if (e.getWheelRotation() < 0)
            cutoff = cutoff.add(delta);
        else
            cutoff = cutoff.sub(delta);

        try {
            spotLight.setConeCutoffAngle(cutoff);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }

        super.mouseWheelMoved(e);
    }

    private void updateLightColor(Color c) {
        spotLight.setDiffuse(c);
        spotLight.setSpecular(c);
        ((Entity) lightNode.getAttachedObject(1)).getSubEntity(0).getMaterial().setEmissive(c);
    }

}
