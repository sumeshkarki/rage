/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.controllers.*;
import ray.rage.util.*;
import ray.rml.*;

public class AsteroidSkyBoxTest extends VariableFrameRateGame {

    private final static float CAMERA_DISTANCE     = 8.0f;

    private boolean            autoRotationEnabled = true;
    private SceneNode          mainCameraNode;
    private SceneNode          asteroidNode;

    public AsteroidSkyBoxTest() {
        super();
    }

    public static void main(String[] args) {
        Game test = new AsteroidSkyBoxTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera mainCamera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        mainCamera.getFrustum().setNearClipDistance(0.1f);
        mainCamera.setViewport(rw.getViewport(0));

        mainCameraNode = sm.getRootSceneNode().createChildSceneNode(mainCamera.getName() + "Node");
        mainCameraNode.attachObject(mainCamera);
        mainCameraNode.moveBackward(CAMERA_DISTANCE);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(new Color(.01f, .01f, .01f));

        setupAsteroid(sm);
        setupSkyBox(engine, sm);
        setupLight(sm);

        OrbitController oc = new OrbitController(asteroidNode);
        oc.addNode(mainCameraNode);
        oc.setDistanceFromTarget(CAMERA_DISTANCE);
        oc.setSpeed(.1f);
        oc.setAlwaysFacingTarget(true);

        RotationController rc = new RotationController(new Vector3f(1.1f, -1.15f, 1.2f));
        rc.addNode(asteroidNode);
        rc.setSpeed(.035f);

        sm.addController(oc);
        sm.addController(rc);
    }

    @Override
    protected void update(Engine engine) {
        for (Node.Controller nc : engine.getSceneManager().getControllers())
            nc.setEnabled(autoRotationEnabled);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_SPACE:
                autoRotationEnabled = !autoRotationEnabled;
                break;
        }
        super.keyPressed(e);
    }

    private void setupSkyBox(Engine engine, SceneManager sm) throws IOException {
        Configuration conf = engine.getConfiguration();
        TextureManager textureMgr = engine.getTextureManager();

        textureMgr.setBaseDirectoryPath(conf.valueOf("assets.skyboxes.path"));
        Texture front = textureMgr.getAssetByPath("front.png");
        Texture back = textureMgr.getAssetByPath("back.png");
        Texture left = textureMgr.getAssetByPath("left.png");
        Texture right = textureMgr.getAssetByPath("right.png");
        Texture top = textureMgr.getAssetByPath("top.png");
        Texture bottom = textureMgr.getAssetByPath("bottom.png");
        textureMgr.setBaseDirectoryPath(conf.valueOf("assets.textures.path"));

        // cubemap textures must be flipped up-side-down to face inward; all
        // textures must have the same dimensions, so any image height will do
        AffineTransform xform = new AffineTransform();
        xform.translate(0, front.getImage().getHeight());
        xform.scale(1d, -1d);

        front.transform(xform);
        back.transform(xform);
        left.transform(xform);
        right.transform(xform);
        top.transform(xform);
        bottom.transform(xform);

        SkyBox sb = sm.createSkyBox("SkyBox");
        sb.setTexture(front, SkyBox.Face.FRONT);
        sb.setTexture(back, SkyBox.Face.BACK);
        sb.setTexture(left, SkyBox.Face.LEFT);
        sb.setTexture(right, SkyBox.Face.RIGHT);
        sb.setTexture(top, SkyBox.Face.TOP);
        sb.setTexture(bottom, SkyBox.Face.BOTTOM);
        sm.setActiveSkyBox(sb);
    }

    private void setupAsteroid(SceneManager sm) throws IOException {
        Entity asteroid = sm.createEntity("Asteroid", "asteroid_3.obj");

        asteroidNode = sm.getRootSceneNode().createChildSceneNode(asteroid.getName() + "Node");
        asteroidNode.attachObject(asteroid);
    }

    private void setupLight(SceneManager sm) throws IOException {
        Light light = sm.createLight("SkyBoxLight", Light.Type.DIRECTIONAL);
        light.setDiffuse(Color.RED);
        light.setSpecular(Color.RED);
        SceneNode lightNode = sm.getRootSceneNode().createChildSceneNode(light.getName() + "Node");
        lightNode.attachObject(light);
        lightNode.setLocalPosition(1, 1, 1);
    }

}
