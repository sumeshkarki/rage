/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene.controllers;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.*;
import ray.rml.*;

public final class TranslationControllerTest extends VariableFrameRateGame {

    private static final float    TRANSLATION_DELTA     = 0.001f;

    private TranslationController translationController = new TranslationController();

    public static void main(String[] args) {
        Game test = new TranslationControllerTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.moveBackward(10);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(Color.LIGHT_GRAY);

        Entity sphere = sm.createEntity("Sphere", "sphere.obj");
        Entity cube = sm.createEntity("Cube", "cube.obj");

        SceneNode rootNode = sm.getRootSceneNode();
        SceneNode sphereNode = rootNode.createChildSceneNode(sphere.getName() + "Node");
        SceneNode cubeNode = rootNode.createChildSceneNode(cube.getName() + "Node");

        sphereNode.attachObject(sphere);
        cubeNode.attachObject(cube);

        sphereNode.moveRight(3);
        cubeNode.moveLeft(3);

        translationController.addNode(sphereNode);
        translationController.addNode(cubeNode);

        sm.addController(translationController);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_1:
                translationController.setTranslationAxis(Vector3f.unitX());
                break;
            case KeyEvent.VK_2:
                translationController.setTranslationAxis(Vector3f.unitY());
                break;
            case KeyEvent.VK_3:
                translationController.setTranslationAxis(Vector3f.unitZ());
                break;
        }
        super.keyPressed(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        final float speed = translationController.getSpeed() + TRANSLATION_DELTA;
        if (e.getWheelRotation() < 0)
            translationController.setSpeed(speed);
        else
            translationController.setSpeed(speed);

        super.mouseWheelMoved(e);
    }

}
