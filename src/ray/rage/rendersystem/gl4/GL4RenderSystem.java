/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.rendersystem.gl4;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.*;
import java.util.logging.*;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.*;

import ray.rage.rendersystem.*;
import ray.rage.rendersystem.Renderable.*;
import ray.rage.rendersystem.shader.*;
import ray.rage.rendersystem.shader.GpuShaderProgram.*;
import ray.rage.rendersystem.shader.glsl.*;
import ray.rage.rendersystem.states.*;
import ray.rage.scene.*;
import ray.rml.*;

/**
 * An OpenGL 4-specific implementation of the {@link RenderSystem render-system}
 * interface, using the <a href="http://jogamp.org/"><b>J</b>ava
 * <b>O</b>pen<b>GL</b></a> bindings to interact with the graphics hardware.
 *
 * @author Raymond L. Rivera
 *
 */
final class GL4RenderSystem implements RenderSystem, GLEventListener {

    private static final Logger         logger             = Logger.getLogger(GL4RenderSystem.class.getName());
    private static final int            INVALID_ID         = -1;

    private int                         vertexArrayObjId   = INVALID_ID;

    private Map<Type, GpuShaderProgram> gpuProgramMap      = new HashMap<>();
    private GpuShaderProgramFactory     gpuProgramFactory  = new GlslProgramFactory();

    private RenderQueue                 renderQueue;
    private Viewport                    activeViewport;

    private Matrix4                     viewMatrix         = Matrix4f.identity();
    private Matrix4                     projMatrix         = Matrix4f.identity();

    private RenderWindow                window;
    private GLCanvas                    canvas;

    private AtomicBoolean               updateRequested    = new AtomicBoolean(false);
    private AtomicBoolean               contextInitialized = new AtomicBoolean(false);

    private RenderSystem.Capabilities   capabilities;

    private List<Light>                 lightsList;
    private AmbientLight                ambientLight;

    public GL4RenderSystem() {
        try {
            final GLCapabilities caps = new GLCapabilities(GLProfile.get(GLProfile.GL4));
            caps.setBackgroundOpaque(true);
            caps.setDoubleBuffered(true);
            caps.setRedBits(8);
            caps.setGreenBits(8);
            caps.setBlueBits(8);
            caps.setAlphaBits(8);
            canvas = new GLCanvas(caps);
            canvas.addGLEventListener(this);
            canvas.setAutoSwapBufferMode(false);
        } catch (GLException e) {
            throw new RuntimeException("Could not create OpenGL 4 context. Check your hardware or drivers", e);
        }
    }

    @Override
    public API getAPI() {
        return API.OPENGL_4;
    }

    @Override
    public Canvas getCanvas() {
        return canvas;
    }

    @Override
    public RenderWindow createRenderWindow(DisplayMode displayMode, boolean fullScreen) {
        if (window == null) {
            window = new GL4RenderWindow(canvas, displayMode, fullScreen);
        }
        return window;
    }

    @Override
    public RenderWindow createRenderWindow(boolean fullScreen) {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        return createRenderWindow(gd.getDisplayMode(), fullScreen);
    }

    @Override
    public RenderQueue createRenderQueue() {
        return new GL4RenderQueue();
    }

    @Override
    public boolean isInitialized() {
        return contextInitialized.get();
    }

    @Override
    public void processRenderQueue(RenderQueue rq, Viewport vp, Matrix4 view, Matrix4 proj) {
        if (rq == null)
            throw new NullPointerException("Null " + RenderQueue.class.getSimpleName());
        if (vp == null)
            throw new NullPointerException("Null " + Viewport.class.getSimpleName());
        if (view == null)
            throw new NullPointerException("Null view matrix");
        if (proj == null)
            throw new NullPointerException("Null projection matrix");

        renderQueue = rq;
        viewMatrix = view;
        projMatrix = proj;
        activeViewport = vp;

        updateRequested.set(true);
        canvas.display();
        updateRequested.set(false);

        // avoid leftovers; these must be reset on every frame
        renderQueue = null;
        viewMatrix = null;
        projMatrix = null;
        activeViewport = null;
    }

    @Override
    public void setAutoSwapBuffer(boolean enabled) {
        canvas.setAutoSwapBufferMode(enabled);
    };

    @Override
    public boolean isAutoSwappingBuffer() {
        return canvas.getAutoSwapBufferMode();
    };

    @Override
    public void swapBuffers() {
        canvas.swapBuffers();
    }

    @Override
    public void init(GLAutoDrawable glad) {
        // glad.setGL(new TraceGL4(new DebugGL4(glad.getGL().getGL4()),
        // System.out));
        GL4 gl = (GL4) glad.getGL();

        capabilities = new GL4RenderSystemCaps(gl);

        // TODO: turn all remaining enable/disable/etc. calls to render states
        // at some point to allow changes after this initialization(?)
        gl.glEnable(GL4.GL_CULL_FACE);
        gl.glEnable(GL4.GL_DEPTH_TEST);
        gl.glDepthFunc(GL4.GL_LEQUAL);
        gl.glEnable(GL4.GL_SCISSOR_TEST);
        gl.glEnable(GL4.GL_PROGRAM_POINT_SIZE);
        gl.glEnable(GL4.GL_TEXTURE_CUBE_MAP_SEAMLESS);

        int[] vaos = new int[1];
        gl.glGenVertexArrays(vaos.length, vaos, 0);
        vertexArrayObjId = vaos[0];

        gl.glBindVertexArray(vertexArrayObjId);

        contextInitialized.set(true);
    }

    @Override
    public void display(GLAutoDrawable glad) {
        // this prevents automatic display invocations during window/context
        // creation from proceeding, which will result in a crash b/c the other
        // assets/dependencies will not be ready/setup before this is
        // auto-invoked the first time; it also prevents attempts to draw before
        // the context has been initialized in its own separate thread
        if (!updateRequested.get() || !contextInitialized.get())
            return;

        GL4 gl = (GL4) glad.getGL();

        // @formatter:off
        gl.glViewport(
            activeViewport.getActualLeft(),
            activeViewport.getActualBottom(),
            activeViewport.getActualWidth(),
            activeViewport.getActualHeight()
        );
        gl.glScissor(
            activeViewport.getActualScissorLeft(),
            activeViewport.getActualScissorBottom(),
            activeViewport.getActualScissorWidth(),
            activeViewport.getActualScissorHeight()
        );
        // @formatter:on

        gl.glClearBufferfv(GL4.GL_COLOR, 0, activeViewport.getClearColorBuffer());
        gl.glClearBufferfv(GL4.GL_DEPTH, 0, activeViewport.getClearDepthBuffer());

        for (Renderable r : renderQueue) {
            GpuShaderProgram program = r.getGpuShaderProgram();
            if (program == null) {
                logger.severe(Renderable.class.getSimpleName() + " skipped. No "
                        + GpuShaderProgram.class.getSimpleName() + " set");
                continue;
            }
            setRenderStates(r);
            final GpuShaderProgram.Context ctx = program.createContext();
            ctx.setRenderable(r);
            ctx.setViewMatrix(viewMatrix);
            ctx.setProjectionMatrix(projMatrix);
            ctx.setLightsList(lightsList);
            ctx.setAmbientLight(ambientLight);

            program.bind();
            program.fetch(ctx);
            drawRenderable(gl, r);
            program.unbind();

            ctx.notifyDispose();
        }
    }

    private void setRenderStates(Renderable r) {
        for (RenderState state : r.getRenderStates())
            if (state.isEnabled())
                state.apply();
    }

    private void drawRenderable(GL4 gl, Renderable r) {
        final DataSource source = r.getDataSource();
        final int primitive = getGLPrimitive(r.getPrimitive());
        switch (source) {
            case INDEX_BUFFER:
                gl.glDrawElements(primitive, r.getIndexBuffer().capacity(), GL4.GL_UNSIGNED_INT, 0);
                break;
            case VERTEX_BUFFER:
                gl.glDrawArrays(primitive, 0, r.getVertexBuffer().capacity());
                break;
            default:
                logger.severe("Draw call skipped. Invalid " + DataSource.class.getName() + ": " + source);
                break;
        }
    }

    @Override
    public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
        // It is sometimes the case (e.g. when FSEM is requested on certain platforms)
        // that this callback will be invoked before the GL4RenderWindow ctor has
        // actually returned, meaning the window ref will still be unassigned/null, so
        // we check here to avoid exceptions.
        if (window == null)
            return;

        for (Viewport vp : window.getViewports())
            vp.notifyDimensionsChanged();
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        GL4 gl = (GL4) glad.getGL();

        gl.glBindVertexArray(0);

        for (GpuShaderProgram program : gpuProgramMap.values())
            program.notifyDispose();
        gpuProgramMap.clear();

        int[] vaos = new int[] { vertexArrayObjId };
        gl.glDeleteVertexArrays(vaos.length, vaos, 0);

        vertexArrayObjId = INVALID_ID;

        contextInitialized = null;
        capabilities = null;
        gpuProgramMap = null;
        gpuProgramFactory = null;
        renderQueue = null;
        projMatrix = null;
        viewMatrix = null;
        window = null;
        lightsList = null;
        ambientLight = null;
    }

    @Override
    public RenderWindow getRenderWindow() {
        return window;
    }

    @Override
    public Capabilities getCapabilities() {
        return capabilities;
    }

    @Override
    public RenderState createRenderState(RenderState.Type type) {
        if (type == null)
            throw new NullPointerException("Null " + RenderState.Type.class.getSimpleName());

        switch (type) {
            case ZBUFFER:
                return new GL4ZBufferState(canvas);
            case TEXTURE:
                // TODO: Add TBO ref to validate buffer IDs, etc
                return new GL4TextureState(capabilities, canvas);
            case FRONT_FACE:
                return new GL4FrontFaceState(canvas);
            default:
                throw new RuntimeException("Unimplemented " + RenderState.Type.class.getSimpleName() + ": " + type);
        }
    }

    @Override
    public GpuShaderProgram createGpuShaderProgram(GpuShaderProgram.Type type) {
        if (gpuProgramMap.containsKey(type))
            throw new RuntimeException(GpuShaderProgram.class.getSimpleName() + " already exists: " + type);

        GpuShaderProgram program = gpuProgramFactory.createInstance(this, type);
        gpuProgramMap.put(type, program);
        return program;
    }

    @Override
    public GpuShaderProgram getGpuShaderProgram(GpuShaderProgram.Type type) {
        GpuShaderProgram program = gpuProgramMap.get(type);

        if (program == null)
            throw new RuntimeException(GpuShaderProgram.class.getSimpleName() + " does not exist: " + type);

        return program;
    }

    @Override
    public void setActiveLights(List<Light> lights) {
        lightsList = lights;
    }

    @Override
    public void setAmbientLight(AmbientLight ambient) {
        ambientLight = ambient;
    }

    @Override
    public void notifyDispose() {
        window.notifyDispose();
        canvas.disposeGLEventListener(this, false);
    }

    private static int getGLPrimitive(Primitive primitive) {
        switch (primitive) {
            case TRIANGLES:
                return GL4.GL_TRIANGLES;
            case TRIANGLE_STRIP:
                return GL4.GL_TRIANGLE_STRIP;
            case LINES:
                return GL4.GL_LINES;
            case POINTS:
                return GL4.GL_POINTS;
            default:
                logger.severe("Unimplemented primitive: " + primitive + ". Using " + Primitive.TRIANGLES);
                return GL4.GL_TRIANGLES;
        }
    }

}
