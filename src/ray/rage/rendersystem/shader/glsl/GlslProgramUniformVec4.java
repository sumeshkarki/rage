/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.rendersystem.shader.glsl;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.*;

import ray.rage.rendersystem.shader.*;
import ray.rml.*;

/**
 * An {@link GpuShaderProgram.Input input} uniform for a 4-component
 * {@link Vector4 vector}.
 * <p>
 * This is intended for GLSL <code>vec4</code> types with the
 * <code>uniform</code> type qualifier.
 *
 * @author Raymond L. Rivera
 *
 */
final class GlslProgramUniformVec4 extends AbstractGlslProgramUniform<Vector4> {

    GlslProgramUniformVec4(GpuShaderProgram parent, GLCanvas canvas, String name) {
        super(parent, canvas, name);
    }

    @Override
    protected void setImpl(GL4 gl, Vector4 v) {
        // location, vec4 object count, values in a vec4, offset
        gl.glUniform4fv(getLocationId(), 1, v.toFloatArray(), 0);
    }

}
