/**
 * Copyright (C) 2016 Raymond L. Rivera
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene.generic;

import java.util.*;

import ray.rage.rendersystem.*;
import ray.rage.scene.*;

/**
 * A generic {@link Camera camera} implementation.
 *
 * @author Raymond L. Rivera
 *
 */
final class GenericCamera extends AbstractGenericSceneObject implements Camera {

    private Viewport              viewport;
    private Camera.Frustum        frustum;
    private List<Camera.Listener> camListeners = new ArrayList<>();

    /**
     * Creates a new {@link Camera camera} with the specified parent
     * {@link SceneManager manager}, name, and {@link Camera.Frustum
     * viewing-frustum}.
     *
     * @param manager
     *            The parent {@link SceneManager manager}.
     * @param camName
     *            The name that identifies the {@link Camera camera}.
     * @param viewFrustum
     *            The viewing {@link Camera.Frustum frustum}.
     * @throws NullPointerException
     *             If any of the arguments is <code>null</code>.
     * @throws IllegalArgumentException
     *             If the name is empty.
     */
    GenericCamera(SceneManager manager, String camName, Camera.Frustum viewFrustum) {
        super(manager, camName);
        if (viewFrustum == null)
            throw new NullPointerException("Null " + Frustum.class.getSimpleName());

        frustum = viewFrustum;
        frustum.notifyCamera(this);
        setVisible(false);
    }

    @Override
    public Camera.Frustum getFrustum() {
        return frustum;
    }

    @Override
    public void renderScene() {
        if (viewport == null)
            throw new IllegalStateException(Viewport.class.getSimpleName() + " not set");

        for (Camera.Listener cl : camListeners)
            cl.onCameraPreRenderScene(this);

        getManager().notifyRenderScene(this, viewport);

        for (Camera.Listener cl : camListeners)
            cl.onCameraPostRenderScene(this);
    }

    @Override
    public void setViewport(Viewport vp) {
        // let the current viewport know it no longer has this camera
        // before re-assigning the reference
        if (viewport != null)
            viewport.notifyCameraChanged(null);

        viewport = vp;
        if (vp != null)
            vp.notifyCameraChanged(this);
    }

    @Override
    public Viewport getViewport() {
        return viewport;
    }

    @Override
    public void addListener(Camera.Listener listener) {
        if (listener == null)
            throw new NullPointerException("Null " + Camera.Listener.class.getSimpleName());

        if (camListeners.contains(listener))
            throw new RuntimeException(Camera.Listener.class.getSimpleName() + " already added");

        camListeners.add(listener);
    }

    @Override
    public void removeListener(Camera.Listener listener) {
        camListeners.remove(listener);
    }

    @Override
    public void notifyDispose() {
        // do not ask the scene manager to destroy this camera from here;
        // the manager is expected to invoke this method and it'll cause an
        // infinite callback loop
        if (viewport != null)
            viewport.notifyCameraChanged(null);

        camListeners.clear();
        super.notifyDispose();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        Node parentNode = getParentNode();

        sb.append(GenericCamera.class.getSimpleName() + "=" + getName());
        sb.append(": Projection=" + frustum.getProjection());
        if (parentNode != null) {
            sb.append(", Position=" + parentNode.getWorldPosition());
            sb.append(", ViewDirection=" + parentNode.getWorldForwardAxis());
            sb.append(", UpDirection=" + parentNode.getWorldUpAxis());
        }
        sb.append(", FoV-Y=" + frustum.getFieldOfViewY());
        sb.append(", Ratio=" + frustum.getAspectRatio());
        sb.append(", NearClip=" + frustum.getNearClipDistance());
        sb.append(", FarClip=" + frustum.getFarClipDistance());

        return sb.toString();
    }

}
