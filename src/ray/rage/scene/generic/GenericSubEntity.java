/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene.generic;

import java.nio.*;

import ray.rage.asset.mesh.*;
import ray.rage.scene.*;
import ray.rml.*;

/**
 * A generic {@link SubEntity} implementation.
 *
 * @author Raymond L. Rivera
 *
 */
final class GenericSubEntity extends AbstractGenericRenderable implements SubEntity {

    private Entity  parentEntity;
    private SubMesh subMesh;

    /**
     * Creates a new {@link SubEntity sub-entity} with the given {@link Entity
     * entity} and {@link SubMesh sub-mesh}.
     *
     * @param parent
     *            The {@link Entity parent} that created <code>this</code>.
     * @param sm
     *            The {@link SubMesh sub-mesh} <code>this</code> is based on.
     * @throws NullPointerException
     *             If either argument is <code>null</code>.
     */
    GenericSubEntity(Entity ent, SubMesh sm) {
        super();
        if (ent == null)
            throw new NullPointerException("Null " + Entity.class.getSimpleName());
        if (sm == null)
            throw new NullPointerException("Null " + SubMesh.class.getSimpleName());

        parentEntity = ent;
        subMesh = sm;
    }

    public Entity getParent() {
        return parentEntity;
    }

    public SubMesh getSubMesh() {
        return subMesh;
    }

    @Override
    public FloatBuffer getVertexBuffer() {
        return subMesh.getVertexBuffer();
    }

    @Override
    public FloatBuffer getTextureCoordsBuffer() {
        return subMesh.getTextureCoordBuffer();
    }

    @Override
    public FloatBuffer getNormalsBuffer() {
        return subMesh.getNormalBuffer();
    }

    @Override
    public IntBuffer getIndexBuffer() {
        return subMesh.getIndexBuffer();
    }

    @Override
    public Matrix4 getWorldTransformMatrix() {
        return parentEntity.getParentSceneNode().getWorldTransform();
    }

    @Override
    public void notifyDispose() {
        parentEntity = null;

        // sub-meshes are owned by meshes, so sub-entities must NOT dispose of
        // them explicitly; nulls are enough
        subMesh = null;

        super.notifyDispose();
    }

}
